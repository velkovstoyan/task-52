import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
});

document.body.addEventListener("click", function() {
  for (let i = 0; i < 5; i++) {
    let article = document.createElement("article");
    article.classList.add("message");
    article.innerHTML = "This is a sample message.";
    document.body.appendChild(article);
  }
});